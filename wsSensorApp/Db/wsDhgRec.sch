[schematic2]
uniq 5
[tools]
[detail]
w 396 67 100 0 n#1 eais.Raw.FLNK 272 64 520 64 outhier.FLNK.p
w 340 35 100 0 n#2 eais.Raw.VAL 272 32 408 32 outhier.TEMP.p
w -40 -157 100 0 n#3 eais.Raw.SLNK 16 48 -112 48 -112 0 -640 0 inhier.SLNK.P
w 26 90 -100 0 n#4 eais.Raw.INP 16 80 16 80 hwinxxl.hwinxxl#3.in
[cell use]
use outhier 377 -5 100 0 TEMP
xform 0 392 32
use outhier 489 28 100 0 FLNK
xform 0 504 64
use inhier -654 -34 100 0 SLNK
xform 0 -640 0
use hwinxxl -656 8 100 0 hwinxxl#3
xform 0 -320 56
p -646 72 100 0 -1 val(in):@wsDhg.proto getTemp($(CHAN)) $(PORT)
use eais 136 -25 100 0 Raw
xform 0 144 48
p 52 -25 100 0 -1 PV:$(top)
p 79 104 100 0 1 DTYP:stream
[comments]
