[schematic2]
uniq 4
[tools]
[detail]
w 296 -165 100 0 n#1 eais.Raw.FLNK 256 -168 344 -168 outhier.FLNK.p
w -396 -229 100 0 n#2 inhier.SLNK.P -656 -232 -136 -232 -136 -184 0 -184 eais.Raw.SLNK
w 2 -149 100 0 n#3 hwinxxl.hwinxxl#4.in -1 -152 -1 -152 eais.Raw.INP
[cell use]
use outhier 315 -205 100 0 FLNK
xform 0 328 -168
use inhier -675 -268 100 0 SLNK
xform 0 -656 -232
use hwinxxl -672 -224 100 0 hwinxxl#4
xform 0 -336 -176
p -662 -160 100 0 -1 val(in):@wsPartMon.proto getPartMon($(top)) $(PORT)
use eais 141 -262 100 0 Raw
xform 0 128 -184
p 57 -262 100 0 -1 PV:$(top)
p 56 -116 100 0 1 SCAN:I/O Intr
p 56 -85 100 0 1 DTYP:stream
[comments]
