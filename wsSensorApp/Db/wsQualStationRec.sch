[schematic2]
uniq 6
[tools]
[detail]
w 192 291 100 0 n#1 eais.getQualDataRaw.FLNK 144 288 240 288 outhier.FLNK.p
w -464 19 100 0 n#2 inhier.SLNK.P -656 16 -272 16 -272 272 -112 272 eais.getQualDataRaw.SLNK
w -110 307 100 0 n#3 hwinxxl.hwinxxl#3.in -113 304 -113 304 eais.getQualDataRaw.INP
w 207 115 100 0 n#4 hwinl.hwinl#11.in 175 112 240 112 outhier.STAT.p
w 199 3 100 0 n#5 hwinl.hwinl#13.in 168 0 240 0 outhier.NSEV.p
[cell use]
use outhier 211 251 100 0 FLNK
xform 0 224 288
use inhier -678 -17 100 0 SLNK
xform 0 -656 16
use hwinxxl -784 232 100 0 hwinxxl#3
xform 0 -448 280
p -774 296 100 0 -1 val(in):@wsQualStation.proto getQualData($(top)) $(PORT) $(ADDR)
use eais -14 199 100 0 getQualDataRaw
xform 0 16 272
p -50 356 100 0 1 DTYP:stream
p -53 333 100 0 1 SCAN:I/O Intr
p -98 199 100 0 -1 PV:$(top)
p 0 192 100 1024 0 name:
use outhier 211 75 100 0 STAT
xform 0 224 112
use outhier 211 -37 100 0 NSEV
xform 0 224 0
use hwinl -216 40 100 0 hwinl#11
xform 0 -16 88
p -198 104 100 0 -1 val(in):$(top)getQualDataRaw.STAT
use hwinl -216 -72 100 0 hwinl#13
xform 0 -16 -24
p -198 -8 100 0 -1 val(in):$(top)getQualDataRaw.NSEV
[comments]
