[schematic2]
uniq 4
[tools]
[detail]
w 312 -149 100 0 n#1 eais.Raw.FLNK 264 -152 360 -152 outhier.FLNK.p
w -388 -197 100 0 n#2 inhier.SLNK.P -648 -200 -128 -200 -128 -168 8 -168 eais.Raw.SLNK
w 10 -133 100 0 n#3 hwinxxl.hwinxxl#3.in 7 -136 7 -136 eais.Raw.INP
[cell use]
use outhier 331 -189 100 0 FLNK
xform 0 344 -152
use inhier -679 -234 100 0 SLNK
xform 0 -648 -200
use hwinxxl -664 -208 100 0 hwinxxl#3
xform 0 -328 -160
p -657 -143 100 0 -1 val(in):@wsVaisHum.proto getHumidity$(proto)($(CHAN),$(top)) $(PORT)
use eais 156 -247 100 0 Raw
xform 0 136 -168
p 72 -247 100 0 -1 PV:$(top)
p 65 -92 100 0 1 DTYP:stream
[comments]
