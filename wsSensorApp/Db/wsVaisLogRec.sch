[schematic2]
uniq 4
[tools]
[detail]
w 304 67 100 0 n#1 eais.Raw.FLNK 256 64 352 64 outhier.FLNK.p
w -394 19 100 0 n#2 inhier.SLNK.P -656 16 -132 16 -132 48 0 48 eais.Raw.SLNK
w 3 83 100 0 n#3 eais.Raw.INP 0 80 0 80 hwinxxl.hwinxxl#3.in
[cell use]
use outhier 323 27 100 0 FLNK
xform 0 336 64
use inhier -677 -24 100 0 SLNK
xform 0 -656 16
use hwinxxl -672 8 100 0 hwinxxl#3
xform 0 -336 56
p -662 72 100 0 -1 val(in):@wsVaisLog.proto getDataLogger($(top)) $(PORT)
use eais 138 -31 100 0 Raw
xform 0 128 48
p 54 -31 100 0 -1 PV:$(top)
p -114 168 100 0 1 DTYP:stream
[comments]
