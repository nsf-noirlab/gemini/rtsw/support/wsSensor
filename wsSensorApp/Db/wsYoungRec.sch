[schematic2]
uniq 5
[tools]
[detail]
w 288 -221 100 0 n#1 eais.Raw.VAL 240 -224 336 -224 outhier.WIND.p
w 288 -189 100 0 n#2 eais.Raw.FLNK 240 -192 336 -192 outhier.FLNK.p
w -104 -205 100 0 n#3 eais.Raw.SLNK -16 -208 -192 -208 -192 -272 -672 -272 inhier.SLNK.P
w -14 -173 100 0 n#4 hwinxxl.hwinxxl#3.in -17 -176 -17 -176 eais.Raw.INP
[cell use]
use outhier 303 -167 100 0 FLNK
xform 0 320 -192
use outhier 307 -258 100 0 WIND
xform 0 320 -224
use inhier -690 -322 100 0 SLNK
xform 0 -672 -272
use hwinxxl -688 -248 100 0 hwinxxl#3
xform 0 -352 -200
p -678 -184 100 0 -1 val(in):@wsYng.proto getYng($(CHAN),$(top)) $(PORT)
use eais 133 -287 100 0 Raw
xform 0 112 -208
p 49 -287 100 0 -1 PV:$(top)
p 47 -146 100 0 1 DTYP:stream
[comments]
