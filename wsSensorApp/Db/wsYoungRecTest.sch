[schematic2]
uniq 5
[tools]
[detail]
w 304 19 100 0 n#1 ewaves.Raw.VAL 256 16 352 16 outhier.WIND.p
w 304 83 100 0 n#2 ewaves.Raw.FLNK 256 80 352 80 outhier.FLNK.p
w 3 83 100 0 n#3 ewaves.Raw.INP 0 80 0 80 hwin.hwin#2.in
w -176 51 100 0 n#4 ewaves.Raw.SLNK 0 48 -352 48 -352 48 inhier.FLNK.P
[cell use]
use outhier 323 43 100 0 FLNK
xform 0 336 80
use outhier 323 -18 100 0 WIND
xform 0 336 16
use hwin -192 64 100 0 hwin#2
xform 0 -96 80
p -305 109 100 0 1 val(in):@wsYoung.proto getYoung($(CHAN),$(top)) $(PORT) $(ADDR)
use ewaves 0 -16 100 0 Raw
xform 0 128 48
p 129 -98 100 0 1 DTYP:stream
p 128 -34 100 0 1 NELM:200
p 128 -66 100 0 1 FTVL:CHAR
p 112 -48 100 1024 1 name:$(top)$(I)
p -84 -16 100 0 -1 PV:$(top)
use eaos 576 144 100 0 SpeedU
xform 0 704 208
p 688 112 100 1024 1 name:$(top)$(I)
use eaos 560 -32 100 0 SpeedV
xform 0 688 32
p 672 -64 100 1024 1 name:$(top)$(I)
use eaos 552 -208 100 0 SpeedW
xform 0 680 -144
p 664 -240 100 1024 1 name:$(top)$(I)
use eaos 1072 -224 100 0 SoS
xform 0 1200 -160
p 1184 -256 100 1024 1 name:$(top)$(I)
use eaos 1080 -48 100 0 Speed3d
xform 0 1208 16
p 1192 -80 100 1024 1 name:$(top)$(I)
use eaos 1096 136 100 0 Speed2d
xform 0 1224 200
p 1208 104 100 1024 1 name:$(top)$(I)
use eaos 1616 128 100 0 WindAz
xform 0 1744 192
p 1728 96 100 1024 1 name:$(top)$(I)
use eaos 1600 -56 100 0 WindEl
xform 0 1728 8
p 1712 -88 100 1024 1 name:$(top)$(I)
use eaos 1592 -232 100 0 Temp
xform 0 1720 -168
p 1704 -264 100 1024 1 name:$(top)$(I)
use inhier -370 -2 100 0 FLNK
xform 0 -352 48
[comments]
